﻿using System.Windows.Input;
using Avalonia.Controls;
using LabWeek6.Models;
using LabWeek6.Views;
using ReactiveUI;

namespace LabWeek6.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly LoteViewModel _loteViewModel;
        private readonly SimultaneaViewModel _simultaneaViewModel;

        private UserControl _currentView;
        
        public MainWindowViewModel(Calculator calculator)
        {
            _loteViewModel = new LoteViewModel(calculator);
            _simultaneaViewModel = new SimultaneaViewModel(calculator);

            ShowLoteViewCommand = ReactiveCommand.Create(() => CurrentView = new Lote { DataContext = _loteViewModel });
            ShowSimultaneoViewCommand = ReactiveCommand.Create(() => CurrentView = new Simultanea { DataContext = _simultaneaViewModel });
            
            CurrentView = new Lote { DataContext = _loteViewModel };
        }

        public UserControl CurrentView
        {
            get => _currentView;
            set => this.RaiseAndSetIfChanged(ref _currentView, value);
        }
        
        public ICommand ShowLoteViewCommand { get; }
        public ICommand ShowSimultaneoViewCommand { get; }
    }
}
