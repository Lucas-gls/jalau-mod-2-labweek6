﻿using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using LabWeek6.Models;

namespace LabWeek6.ViewModels
{
    public class SimultaneaViewModel : INotifyPropertyChanged
    {
        private Calculator _calculator;

        public event PropertyChangedEventHandler? PropertyChanged;

        public Calculator Calculator
        {
            get => _calculator;
            set
            {
                if (_calculator != value)
                {
                    _calculator = value;
                    OnPropertyChanged(nameof(Calculator));
                }
            }
        }

        private string _number;

        public string Number
        {
            get => _number;
            set
            {
                if (_number != value)
                {
                    _number = value;
                    OnPropertyChanged(nameof(Number));
                }
            }
        }

        public SimultaneaViewModel(Calculator calculator)
        {
            Calculator = calculator;
            PerformCalculationsInParallel();
        }
        
        public async Task PerformCalculationsInParallel()
        {
            var stop = Stopwatch.StartNew();

            await Task.Run(() => OperationParallel(stop, _calculator));

            stop.Stop();
        }

        private void OperationParallel(Stopwatch stopwatch, Calculator calculator)
        {
            Number = $"{calculator.Sum().ToString(CultureInfo.InvariantCulture)}    Milissegundos: {stopwatch.ElapsedMilliseconds.ToString()}";
            
            Number += $"\n{calculator.Subtraction().ToString(CultureInfo.InvariantCulture)}    Milissegundos: {stopwatch.ElapsedMilliseconds.ToString()}";
            
            Number += $"\n{calculator.Multiplication().ToString(CultureInfo.InvariantCulture)}    Milissegundos: {stopwatch.ElapsedMilliseconds.ToString()}";

            Number += $"\n{calculator.Division().ToString(CultureInfo.InvariantCulture)}     Milissegundos: {stopwatch.ElapsedMilliseconds.ToString()}";
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}