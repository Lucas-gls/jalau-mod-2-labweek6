﻿using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using LabWeek6.Models;

namespace LabWeek6.ViewModels;

public class LoteViewModel
{
    private Calculator _calculator;
    public Calculator Calculator { get; set; }
    
    
    public string _number;


    public string Number
    {
        get => _number;
        set
        {
            _number = value;
            OnPropertyChanged(nameof(Number));
        }
    }
    
    public LoteViewModel(Calculator calculator)
    {
        _calculator = calculator;
        PerformCalculationsInParallel();
    }
    
    public async Task PerformCalculationsInParallel()
    {
        var stop = Stopwatch.StartNew();

        await Task.Run(() => OperationParallel(stop, _calculator));

        stop.Stop();
    }

    private void OperationParallel(Stopwatch stopwatch, Calculator calculator)
    {
        stopwatch.Start();
        Number = $"{calculator.Sum().ToString(CultureInfo.InvariantCulture)}";
        Thread.Sleep(1000);
        
        Number += $"\n{calculator.Subtraction().ToString(CultureInfo.InvariantCulture)}";
        Thread.Sleep(1000);
        
        Number += $"\n{calculator.Multiplication().ToString(CultureInfo.InvariantCulture)}";
        stopwatch.Stop();
        Thread.Sleep(1000);
        
        Number += $"\n{calculator.Division().ToString(CultureInfo.InvariantCulture)}";
        stopwatch.Stop();

    }
   
    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}