﻿namespace LabWeek6.Models;

public class Calculator
{
    public float ValueA { get; set; }
    public float ValueB { get; set; }
    public float ValueC { get; set; }
    public float ValueD { get; set; }
    public float ValueE { get; set; }
    public float ValueF { get; set; }
    public float ValueG { get; set; }
    public float ValueH { get; set; }

    public float Sum()
    {
        return ValueA + ValueB;
    }
    
    public float Subtraction()
    {
        return ValueC - ValueD;
    }
    
    public float Multiplication()
    {
        return ValueE * ValueF;
    }
    
    public float Division()
    {
        return ValueG / ValueH;
    }


}