using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using LabWeek6.Models;
using LabWeek6.ViewModels;
using LabWeek6.Views;

namespace LabWeek6
{
    public partial class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                var calculator = new Calculator {ValueA = 5, ValueB = 2, ValueC = 3, ValueD = 5, ValueE = 10, ValueF = 55, ValueG = 22, ValueH = 12};
                desktop.MainWindow = new MainWindow
                {
                    DataContext = new MainWindowViewModel(calculator),
                };
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}